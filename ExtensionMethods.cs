﻿/*
Copyright (c) 2014, John Lewin
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
*/

using MatterHackers.PolygonMesh;
using MatterHackers.VectorMath;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MatterHackers.MatterControl.Plugins.Lithophane
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Extend PolyMesh.Mesh with a SaveToBinarySTL method
        /// </summary>
        /// <param name="mesh">The mesh to operate on</param>
        /// <param name="filePath">The path to write the binary file to</param>
        public static void SaveToBinarySTL(this Mesh mesh, string filePath)
        {
            using (StreamWriter stream = new StreamWriter(filePath))
            {
                var writer = new BinaryWriter(stream.BaseStream);
                Byte[] header = new Byte[80];

                // Write the 80 byte header
                writer.Write(header);

                // Write "the number of triangular facets in the file"
                writer.Write(mesh.meshEdges.Count);

                foreach (Face face in mesh.Faces)
                {
                    List<Vector3> positionsCCW = new List<Vector3>();
                    foreach (FaceEdge faceEdge in face.FaceEdges())
                    {
                        positionsCCW.Add(faceEdge.firstVertex.Position);
                    }

                    int numPolys = positionsCCW.Count - 2;
                    int secondIndex = 1;
                    int thirdIndex = 2;

                    for (int polyIndex = 0; polyIndex < numPolys; polyIndex++)
                    {
                        // Write normals
                        writer.Write((float)face.normal.x);
                        writer.Write((float)face.normal.y);
                        writer.Write((float)face.normal.z);

                        // Write first triangle
                        writer.Write((float)(positionsCCW[0].x));
                        writer.Write((float)(positionsCCW[0].y));
                        writer.Write((float)(positionsCCW[0].z));

                        // Write second triangle
                        writer.Write((float)(positionsCCW[secondIndex].x));
                        writer.Write((float)(positionsCCW[secondIndex].y));
                        writer.Write((float)(positionsCCW[secondIndex].z));

                        // Write third triangle
                        writer.Write((float)(positionsCCW[thirdIndex].x));
                        writer.Write((float)(positionsCCW[thirdIndex].y));
                        writer.Write((float)(positionsCCW[thirdIndex].z));

                        // Write the "attribute byte count"
                        writer.Write((short)0);

                        secondIndex = thirdIndex;
                        thirdIndex++;
                    }
                }
            }
        }
    }
}