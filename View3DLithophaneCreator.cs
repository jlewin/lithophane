﻿/*
Copyright (c) 2014, John Lewin
Copyright (c) 2014, Lars Brubaker
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;

using SysDraw = System.Drawing;

using ClipperLib;

using MatterHackers.Agg;
using MatterHackers.Agg.Font;
using MatterHackers.Agg.Image;
using MatterHackers.Agg.ImageProcessing;
using MatterHackers.Agg.OpenGlGui;
using MatterHackers.Agg.UI;
using MatterHackers.Agg.VertexSource;
using MatterHackers.MarchingSquares;
using MatterHackers.MatterControl;
using MatterHackers.MatterControl.DataStorage;
using MatterHackers.MatterControl.PartPreviewWindow;
using MatterHackers.MatterControl.PrintLibrary;
using MatterHackers.MatterControl.PrintQueue;
using MatterHackers.MeshVisualizer;
using MatterHackers.PolygonMesh;
using MatterHackers.PolygonMesh.Csg;
using MatterHackers.PolygonMesh.Processors;
using MatterHackers.RayTracer;
using MatterHackers.RayTracer.Traceable;
using MatterHackers.RenderOpenGl;
using MatterHackers.VectorMath;

using OpenTK.Graphics.OpenGL;
using MatterHackers.Localizations;
using MatterHackers.Agg.PlatformAbstract;

namespace MatterHackers.MatterControl.Plugins.Lithophane
{
    public class View3DLithophaneCreator : PartPreview3DWidget
    {
        Slider pixPerMMSlider;
        Slider plateWidthSlider;
        Slider plateHeightSlider;

        ThumbnailWidget lithoThumb;
        Button insertImageButton;

        CheckBox invertImage;
        CheckBox optionsExpander;

        ProgressControl processingProgressControl;

        Button generateButton;
        Button closeButton;
        Button queueButton;

        String imageFileName;

        private Mesh generatedMesh;
        
        SysDraw.Bitmap activeImage;

        void InsertImageButtonOnIdle(object state)
        {
            string selectInstruction = "Select an Image".Localize();
            OpenFileDialogParams openParams = new OpenFileDialogParams("{0}|*.jpg;*.png;*.jpeg".FormatWith(selectInstruction), multiSelect: false);

            FileDialog.OpenFileDialog(ref openParams);
            if (!string.IsNullOrWhiteSpace(openParams.FileName))
            {
                this.activeImage = SysDraw.Image.FromFile(openParams.FileName) as SysDraw.Bitmap; //  img = loadImage(name);//load image
                this.imageFileName = Path.GetFileName(openParams.FileName);

                var thumbImage = Lithophane.ToResizedGrayscale(activeImage, SysDraw.Drawing2D.InterpolationMode.Default, (int) this.lithoThumb.Width);

                string thumbFolder = Path.Combine(ApplicationDataStorage.Instance.ApplicationUserDataPath, "data", "temp", "thumbnails");
                string thumbPath = Path.Combine(thumbFolder, "litho_thumb.jpg");

                Console.WriteLine(thumbPath);
                thumbImage.Save(thumbPath);

                ImageBuffer thumbNail = new ImageBuffer();

                var thumbIB = ImageIO.LoadImageData(thumbPath, thumbNail);
                this.lithoThumb.SetThumbnail(thumbNail);

                lithoThumb.Visible = true;
                optionsExpander.Visible = true;
                optionsExpander.Checked = true;

                insertImageButton.Visible = false;
            }
        }

        public View3DLithophaneCreator(Vector3 viewerVolume, Vector2 bedCenter, MeshViewerWidget.BedShape bedShape)
        {
            string staticDataPath = DataStorage.ApplicationDataStorage.Instance.ApplicationStaticDataPath;

            FlowLayoutWidget mainContainerTopToBottom = new FlowLayoutWidget(FlowDirection.TopToBottom);
            mainContainerTopToBottom.HAnchor = Agg.UI.HAnchor.Max_FitToChildren_ParentWidth;
            mainContainerTopToBottom.VAnchor = Agg.UI.VAnchor.Max_FitToChildren_ParentHeight;

            FlowLayoutWidget centerPartPreviewAndControls = new FlowLayoutWidget(FlowDirection.LeftToRight);
            centerPartPreviewAndControls.AnchorAll();

            GuiWidget viewArea = new GuiWidget();
            viewArea.AnchorAll();
            {
                meshViewerWidget = new MeshViewerWidget(viewerVolume, bedCenter, bedShape);
                meshViewerWidget.AlwaysRenderBed = true;
                meshViewerWidget.AnchorAll();
            }
            viewArea.AddChild(meshViewerWidget);

            centerPartPreviewAndControls.AddChild(viewArea);
            mainContainerTopToBottom.AddChild(centerPartPreviewAndControls);

            FlowLayoutWidget bottomPanel = new FlowLayoutWidget(FlowDirection.LeftToRight);
            bottomPanel.HAnchor = HAnchor.ParentLeftRight;
            bottomPanel.Padding = new BorderDouble(3, 3);
            bottomPanel.BackgroundColor = ActiveTheme.Instance.PrimaryBackgroundColor;

            buttonRightPanel = CreateRightPanel(viewerVolume.y);

            processingProgressControl = new ProgressControl("...", ActiveTheme.Instance.PrimaryTextColor, ActiveTheme.Instance.PrimaryAccentColor);
            bottomPanel.AddChild(processingProgressControl);

            GuiWidget rightPanelHolder = new GuiWidget(HAnchor.FitToChildren, VAnchor.ParentBottomTop);
            centerPartPreviewAndControls.AddChild(rightPanelHolder);
            rightPanelHolder.AddChild(buttonRightPanel);

            viewControls3D = new ViewControls3D(meshViewerWidget);

            // Used to disable access to rightPanel controls
            buttonRightPanelDisabledCover = new Cover(HAnchor.ParentLeftRight, VAnchor.ParentBottomTop);
            buttonRightPanelDisabledCover.BackgroundColor = new RGBA_Bytes(ActiveTheme.Instance.PrimaryBackgroundColor, 150);
            rightPanelHolder.AddChild(buttonRightPanelDisabledCover);
            LockEditControls();

            GuiWidget leftRightSpacer = new GuiWidget();
            leftRightSpacer.HAnchor = HAnchor.ParentLeftRight;
            bottomPanel.AddChild(leftRightSpacer);

            // Save to STL and add to Print Queue
            queueButton = textImageButtonFactory.Generate("Add Item");
            queueButton.Click += GenerateSTL;
            queueButton.Enabled = false;
            bottomPanel.AddChild(queueButton);

            closeButton = textImageButtonFactory.Generate("Cancel");
            closeButton.Click += onCloseButton_Click;
            bottomPanel.AddChild(closeButton);

            mainContainerTopToBottom.AddChild(bottomPanel);

            this.AddChild(mainContainerTopToBottom);
            this.AnchorAll();

            meshViewerWidget.TrackballTumbleWidget.TransformState = TrackBallController.MouseDownType.Rotation;

            AddChild(viewControls3D);

            // set the view to be a good angle and distance
            meshViewerWidget.TrackballTumbleWidget.TrackBallController.Scale = .06;
            meshViewerWidget.TrackballTumbleWidget.TrackBallController.Rotate(Quaternion.FromEulerAngles(new Vector3(-MathHelper.Tau * .02, 0, 0)));

            UnlockEditControls();

            // Reset visibility on rightPanel cover object after UnlockEditControls call
            buttonRightPanelDisabledCover.Visible = false; // true;
        }
        
        private void GenerateMesh()
        {
            // TODO: Add sanity check on values

            queueButton.Enabled = true;

            processingProgressControl.textWidget.Text = "Processing Vertices";
            processingProgressControl.Visible = true;
            processingProgressControl.PercentComplete = 0;
            LockEditControls();

            var lithoWorker = new BackgroundWorker();
            lithoWorker.WorkerReportsProgress = true;

            lithoWorker.DoWork += new DoWorkEventHandler(lithoWorker_DoWork);
            lithoWorker.ProgressChanged += new ProgressChangedEventHandler(lithoWorker_ProgressChanged);
            lithoWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(lithoWorkerCompleted);

            lithoWorker.RunWorkerAsync();
        }
        
        protected Slider InseretUiForSlider2(FlowLayoutWidget container, string labelText, string txtFormatter, double min = 0, double max = .5)
        {
            double scrollBarWidth = 100;

            FlowLayoutWidget descriptionAndNumberEdit = new FlowLayoutWidget(FlowDirection.LeftToRight);
            container.AddChild(descriptionAndNumberEdit);

            TextWidget sliderLabel = new TextWidget(labelText, textColor: ActiveTheme.Instance.PrimaryTextColor);
            sliderLabel.HAnchor = HAnchor.ParentLeftRight;
            sliderLabel.Margin = new BorderDouble(10, 3, 80-sliderLabel.Width, 5);
            sliderLabel.AutoExpandBoundsToText = false;
            descriptionAndNumberEdit.AddChild(sliderLabel);

            var valueEditWidget = new TextEditWidget();
            valueEditWidget.HAnchor = HAnchor.ParentLeftRight;
            valueEditWidget.MinimumSize = new Vector2(45, 10);
            //txtBox.Margin = new BorderDouble(left: 80 - label.LocalBounds.Right);
            descriptionAndNumberEdit.AddChild(valueEditWidget);

            Slider actualSlider = new Slider(new Vector2(), scrollBarWidth, 0, 1);
            actualSlider.Minimum = min;
            actualSlider.Maximum = max;
            actualSlider.Margin = new BorderDouble(3, 5, 3, 3);
            actualSlider.HAnchor = HAnchor.ParentCenter;
            actualSlider.View.BackgroundColor = new RGBA_Bytes();
            container.AddChild(actualSlider);

            
            actualSlider.ValueChanged += (sender, e) =>
            {
                valueEditWidget.Text = actualSlider.Value.ToString(txtFormatter);
            };

            valueEditWidget.TextChanged += (sender, e) =>
            {
                double val;

                // If the user entered a value value and the namedSlider is not already at that value, set it
                if (double.TryParse(valueEditWidget.Text, out val) && actualSlider.Value != val)
                {
                    // Allow for maximum size to be overridden
                    if (actualSlider.Maximum < val)
                    {
                        actualSlider.Maximum = val;
                    }

                    actualSlider.Value = val;
                }
            };

            return actualSlider;
        }

        void lithoWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            this.viewControls3D.Visible = false;

            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            BackgroundWorker backgroundWorker = (BackgroundWorker)sender;

            var generateTimer = Stopwatch.StartNew();
            generatedMesh = Lithophane.Generate(
               this.activeImage,
                (int)plateWidthSlider.Value,
                plateHeightSlider.Value,
                0.4,
                pixPerMMSlider.Value,
                invertImage.Checked,
                backgroundWorker);

            Console.WriteLine("ProcessImage overall: {0}", generateTimer.ElapsedMilliseconds);
        }

        void lithoWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Console.WriteLine(e.ProgressPercentage);

            processingProgressControl.PercentComplete = e.ProgressPercentage;
        }

        void lithoWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var stp1 = Stopwatch.StartNew();
            var meshTranslation = Matrix4X4.Identity;

            UnlockEditControls();

            if (generatedMesh.Faces.Count > 0)
            {
                //PlaceMeshOnBed(GeneratedMesh, ref meshTransform, true);
                var bounds = generatedMesh.GetAxisAlignedBoundingBox(meshTranslation);
                var boundsCenter = (bounds.maxXYZ + bounds.minXYZ) / 2;
                meshTranslation *= Matrix4X4.CreateTranslation(-boundsCenter + new Vector3(0, 0, bounds.ZSize / 2));
                ScaleRotateTranslate newTransform = ScaleRotateTranslate.Identity();
                newTransform.translation = meshTranslation;

                // Update viewer with new mesh
                meshViewerWidget.MeshTransforms.Clear();
                meshViewerWidget.MeshTransforms.Add(newTransform);
                meshViewerWidget.Meshes.Clear();
                meshViewerWidget.Meshes.Add(generatedMesh);
                meshViewerWidget.SelectedMeshIndex = 0;

                Console.WriteLine("Mesh22 transform and add: {0}", stp1.ElapsedMilliseconds);
            }
        }

        void arrangePartsBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UnlockEditControls();
            generateButton.Visible = true;
            viewControls3D.partSelectButton.ClickButton(null);
        }

        void LockEditControls()
        {
            buttonRightPanelDisabledCover.Visible = true;

            viewControls3D.PartSelectVisible = false;
            if (meshViewerWidget.TrackballTumbleWidget.TransformState == TrackBallController.MouseDownType.None)
            {
                viewControls3D.rotateButton.ClickButton(null);
            }
        }

        void UnlockEditControls()
        {
            buttonRightPanelDisabledCover.Visible = false;
            processingProgressControl.Visible = false;

            viewControls3D.PartSelectVisible = true;
        }

        private FlowLayoutWidget CreateRightPanel(double buildHeight)
        {
            FlowLayoutWidget rightPanel = new FlowLayoutWidget(FlowDirection.TopToBottom);

            rightPanel.Width = 240;
            {
                BorderDouble buttonMargin = new BorderDouble(top: 3);

                insertImageButton = whiteButtonFactory.Generate("Select Image", centerText: true);
                insertImageButton.Cursor = Cursors.Hand;
                insertImageButton.Click += (sender, e) =>
                {
                    UiThread.RunOnIdle(InsertImageButtonOnIdle);
                };

                rightPanel.AddChild(insertImageButton);

                lithoThumb = new ThumbnailWidget();
                lithoThumb.Width = insertImageButton.Width;
                lithoThumb.Visible = false;
                lithoThumb.Margin = new BorderDouble(bottom: 2);
                rightPanel.AddChild(lithoThumb);


                // Add options
                {
                    optionsExpander = expandMenuOptionFactory.GenerateCheckBoxButton("Options", "icon_arrow_right_no_border_32x32.png", "icon_arrow_down_no_border_32x32.png");
                    optionsExpander.Margin = new BorderDouble(bottom: 2);
                    optionsExpander.Visible = false;
                    rightPanel.AddChild(optionsExpander);

                    var optionsContainer = new FlowLayoutWidget(FlowDirection.TopToBottom);
                    optionsContainer.HAnchor = HAnchor.ParentLeftRight;
                    optionsContainer.Visible = false;
                    rightPanel.AddChild(optionsContainer);

                    pixPerMMSlider = InseretUiForSlider2(optionsContainer, "Pix/mm:", "#.#", 0.5, 3);
                    pixPerMMSlider.Value = 1.5;

                    // TODO: Slider for width should probably never be greater than bed width
                    plateWidthSlider = InseretUiForSlider2(optionsContainer, "Width:", "#", 0, 180);
                    plateWidthSlider.Value = 150;

                    plateHeightSlider = InseretUiForSlider2(optionsContainer, "Z-Height:", "#.00", .05, 3);
                    plateHeightSlider.Value = 2.5;
                       
                    invertImage = new CheckBox(new CheckBoxViewText("Invert", textColor: ActiveTheme.Instance.PrimaryTextColor));
                    invertImage.Checked = true;
                    invertImage.Margin = new BorderDouble(10, 5);
                    invertImage.HAnchor = HAnchor.ParentLeft;
                    optionsContainer.AddChild(invertImage);

                    // Ensure the child container is visible when the expander is checked
                    optionsExpander.CheckedStateChanged += (sender, e) =>
                    {
                        optionsContainer.Visible = optionsExpander.Checked;
                    };

                    generateButton = whiteButtonFactory.Generate("Generate", centerText: true);
                    generateButton.Margin = new BorderDouble(top: 8);
                    generateButton.Cursor = Cursors.Hand;
                    generateButton.Click += (sender, e) => GenerateMesh();
                    optionsContainer.AddChild(generateButton);
                }
                
                var verticalSpacer = new GuiWidget();
                verticalSpacer.VAnchor = VAnchor.ParentBottomTop;
                rightPanel.AddChild(verticalSpacer);

            }

            rightPanel.Padding = new BorderDouble(6, 6);
            rightPanel.Margin = new BorderDouble(0, 1);
            rightPanel.BackgroundColor = ActiveTheme.Instance.PrimaryBackgroundColor;
            rightPanel.VAnchor = VAnchor.ParentBottomTop;

            return rightPanel;
        }
        
        private void GenerateSTL(object sender, EventArgs e)
        {
            if (this.generatedMesh != null)
            {
                processingProgressControl.textWidget.Text = "Saving Parts:";
                processingProgressControl.Visible = true;
                processingProgressControl.PercentComplete = 0;
                LockEditControls();

                BackgroundWorker stlWorker = new BackgroundWorker();
                stlWorker.WorkerReportsProgress = true;

                stlWorker.DoWork += new DoWorkEventHandler(GenerateStlWorker_DoWork);
                stlWorker.ProgressChanged += new ProgressChangedEventHandler(GenerateStlWorker_ProgressChanged);
                stlWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GenerateStlWorker_RunWorkerCompleted);

                stlWorker.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Persist the generated mesh to disk
        /// </summary>
        /// <param name="sender">The BackgroundWorker</param>
        /// <param name="e">The event args</param>
        void GenerateStlWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            BackgroundWorker backgroundWorker = (BackgroundWorker)sender;
            try
            {
                string stlPath;
                int key = 1;

                // Create a unique filename
                do
                {
                    stlPath = Path.Combine(
                        ApplicationDataStorage.Instance.ApplicationLibraryDataPath, 
                        string.Format("Lithophane{0}.stl", key++));


                } while(File.Exists(stlPath));

                var stp1 = Stopwatch.StartNew();

                // StlProcessing.Save(asynchMeshesList[0], Path.ChangeExtension(stlPath, "alpha.stl"));
                generatedMesh.SaveToBinarySTL(stlPath);
                Console.WriteLine("Binary Write Overall: {0}", stp1.ElapsedMilliseconds);

                e.Result = stlPath;
            }
            catch (System.UnauthorizedAccessException)
            {
                //Do something special when unauthorized?
                StyledMessageBox.ShowMessageBox("Oops! Unable to save changes.", "Unable to save");
            }
            catch
            {
                StyledMessageBox.ShowMessageBox("Oops! Unable to save changes.", "Unable to save");
            }
        }


        void GenerateStlWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            processingProgressControl.PercentComplete = e.ProgressPercentage;
        }

        /// <summary>
        /// Adds a new PrintItem to the print queue for the newly generated mesh
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void GenerateStlWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var stp1 = Stopwatch.StartNew();
            string filePath = e.Result as string;
            if (filePath != null)
            {
                PrintItem printItem = new PrintItem();
                printItem.Commit();

                printItem.Name = Path.GetFileNameWithoutExtension(filePath);
                printItem.FileLocation = Path.GetFullPath(filePath);
                printItem.PrintItemCollectionID = LibraryData.Instance.LibraryCollection.Id;
                printItem.Commit();

                PrintItemWrapper printItemWrapper = new PrintItemWrapper(printItem);

                LibraryData.Instance.AddItem(printItemWrapper);

                // and save to the queue
                {
                    QueueData.Instance.AddItem(printItemWrapper);
                }

                Console.WriteLine("Generate Print Item Wrapper: {0}", stp1.ElapsedMilliseconds);

            }

            //Exit after save
            Close();
        }

        private void onCloseButton_Click(object sender, EventArgs e)
        {
            UiThread.RunOnIdle((o) => Close());
        }
    }       
}
